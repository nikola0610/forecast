Weather service
=======

### Installation 

Installing dependencies
```php
composer install
```

Create database
```php
php bin/console doctrine:database:create
```

Create database tables
```php
doctrine:migrations:diff 
doctrine:migrations:migrate
```

Load fixtures
```php
php bin/console doctrine:fixtures:load
```

Collect weather data
```php
php bin/console app:collect-weather-data
```

### Usage

Resources:

Static content
```php
METHOD: GET
PATH: /
```

Find average temperature for period
```php
METHOD: GET
PATH: /average-temperature
PARAMS: 
     {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-12"},
```

Plan best route for weekend for specific period
```php
METHOD: GET
PATH: /weekend-planner
PARAMS: 
     {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-12"},
```

Weather details
```php
METHOD: GET
PATH: /weather-details
PARAMS: 
      {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
      {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
      {"name"="temperature", "dataType"="string", "required"=false, "description"="less or higher"},
      {"name"="temperature_direction", "dataType"="string", "required"=false, "description"="decimal number"},
```

List of countries
```php
METHOD: GET
PATH: /countries
```

List of cities
```php
METHOD: GET
PATH: /cities
```