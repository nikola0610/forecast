<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Weather
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WeatherRepository")
 * @ORM\Table(name="weather")
 */
class Weather
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"weather"})
     */
    protected $id;

    /**
     * Temperature
     *
     * @ORM\Column(type="decimal", precision=5, scale=2)
     *
     * @Groups({"weather"})
     */
    protected $temperature;

    /**
     * Minimal temperature
     *
     * @ORM\Column(type="decimal", precision=5, scale=2)
     *
     * @Groups({"weather"})
     */
    protected $maxTemperature;

    /**
     * Maximal temperature
     *
     * @ORM\Column(type="decimal", precision=5, scale=2)
     *
     * @Groups({"weather"})
     */
    protected $minTemperature;

    /**
     * Date
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"weather"})
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City", inversedBy="weatherDetails")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @Groups({"weather","temperature"})
     */
    protected $city;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $temperature
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
    }

    /**
     * @return mixed
     */
    public function getMaxTemperature()
    {
        return $this->maxTemperature;
    }

    /**
     * @param mixed $maxTemperature
     */
    public function setMaxTemperature($maxTemperature)
    {
        $this->maxTemperature = $maxTemperature;
    }

    /**
     * @return mixed
     */
    public function getMinTemperature()
    {
        return $this->minTemperature;
    }

    /**
     * @param mixed $minTemperature
     */
    public function setMinTemperature($minTemperature)
    {
        $this->minTemperature = $minTemperature;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date ? $this->date->format('Y-m-d') : $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


}