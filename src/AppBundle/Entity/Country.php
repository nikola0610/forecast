<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Country
 *
 * @ORM\Entity()
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"country","city"})
     */
    protected $id;


    /**
     * Name
     *
     * @Groups({"country","city", "weather", "temperature"})
     *
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    protected $name;

    /**
     * Name
     *
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    protected $nameCanonical;

    /**
     * Code
     *
     * @ORM\Column(type="string", length=60, nullable=false)
     *
     * @Groups({"country","city", "weather"})
     */
    protected $countryCode;


    /**
     * Followers
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\City", mappedBy = "country")
     **/
    protected $cities;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNameCanonical()
    {
        return $this->nameCanonical;
    }

    /**
     * @param mixed $nameCanonical
     */
    public function setNameCanonical($nameCanonical)
    {
        $this->nameCanonical = $nameCanonical;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param mixed $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }
}