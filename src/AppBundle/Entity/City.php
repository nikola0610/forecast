<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class City
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository")
 * @ORM\Table(name="city")
 */
class City
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"city"})
     */
    protected $id;

    /**
     * Name
     *
     * @Groups({"city","weather","temperature"})
     *
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    protected $name;

    /**
     * Name
     *
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    protected $nameCanonical;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country", inversedBy="cities")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @Groups({"city", "weather", "temperature"})
     */
    protected $country;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Weather", mappedBy = "city")
     *
     */
    protected $weatherDetails;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNameCanonical()
    {
        return $this->nameCanonical;
    }

    /**
     * @param mixed $nameCanonical
     */
    public function setNameCanonical($nameCanonical)
    {
        $this->nameCanonical = $nameCanonical;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getWeatherDetails()
    {
        return $this->weatherDetails;
    }

    /**
     * @param mixed $weatherDetails
     */
    public function setWeatherDetails($weatherDetails)
    {
        $this->weatherDetails = $weatherDetails;
    }
}