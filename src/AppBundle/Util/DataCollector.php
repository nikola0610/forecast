<?php

namespace AppBundle\Util;

use GuzzleHttp\Client;

/**
 * Class DaraCollector
 * @package AppBundle\Util
 */
class DataCollector
{

    /**
     * @param $countryCode
     * @param $cityName
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getWeatherDataForLocation($countryCode, $cityName)
    {
        $baseUrl = 'https://yoc-media.github.io/weather/report';
        $url = $baseUrl.'/'.$countryCode.'/'.$cityName.'.json';
        $client = new Client();

        return $client->request('GET', $url);
    }
}