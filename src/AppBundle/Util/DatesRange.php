<?php

namespace AppBundle\Util;

/**
 * Class DatesRange
 * @package AppBundle\Util
 */
class DatesRange
{

    /**
     * @param $startDate
     * @param $endDate
     * @return array
     * @throws \Exception
     */
    public static function weekendFinder($startDate, $endDate)
    {
        $weekends = [];
        $startPoint = $startDate ? $startDate : '-7 days';

        $start = new \DateTime($startPoint);
        $end = new \DateTime($endDate);

        $interval = new \DateInterval('P1D');

        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $day) {
            if ($day->format('N') == 6) {
                $weekends[] = [$day->format('Y-m-d'), $day->modify('+1 day')->format('Y-m-d')];
            }
        }

        return $weekends;
    }
}