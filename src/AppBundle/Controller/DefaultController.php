<?php

namespace AppBundle\Controller;

use AppBundle\Form\WeatherDataFilterFormType;
use AppBundle\Form\DateRangeFilterFormType;
use AppBundle\Service\Pager;
use AppBundle\Util\DatesRange;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\View;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends FOSRestController
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="homepage", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="Greeting message",
     *  authentication=false,
     *  statusCodes={
     *    200="Returns list of average weather data",
     *  }
     * )
     */
    public function indexAction(Request $request)
    {
        $view = $this->view(['hello' => 'world!'], 200);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Pager $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/average-temperature", name="average_temperature", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="Find average temperature for period",
     *  authentication=false,
     *  parameters={
     *      {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     *      {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-12"},
     *  },
     *  statusCodes={
     *    200="Returns list weather data",
     *    400="Form errors"
     *  }
     * )
     */
    public function averageTemperatureAction(Request $request, Pager $paginator)
    {
        $context = new Context();
        $context->setGroups(['temperature']);
        $context->enableMaxDepth();

        $form = $form = $this->createForm(DateRangeFilterFormType::class);
        $form->handleRequest($request);

        if ($form->isValid() || !$form->isSubmitted()) {
            $weather = $this->getDoctrine()->getRepository('AppBundle:Weather')->listAverageTemperature(
                $form->getData()
            );
            $data = $paginator->paginate($weather, 2);
            $view = $this->view($data, 200)->setContext($context);
        } elseif ($form->isSubmitted()) {
            $view = $this->view($form);
        }

        return $this->handleView($view);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     *
     * @Route("/weekend-planner", name="weekend_planner", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="Plan best route for weekend for specific period",
     *  authentication=false,
     *  parameters={
     *      {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     *      {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     *  },
     *  statusCodes={
     *    200="Returns list of weekends and corresponding city",
     *    400="Form errors"
     *  }
     * )
     */
    public function weekendPlannerAction(Request $request)
    {
        $context = new Context();
        $context->setGroups(['temperature']);
        $context->enableMaxDepth();

        $form = $form = $this->createForm(DateRangeFilterFormType::class);
        $form->handleRequest($request);

        if ($form->isValid() || !$form->isSubmitted()) {
            $data = [];
            $weekends = DatesRange::weekendFinder($form['start_date']->getData(), $form['end_date']->getData());
            foreach ($weekends as $weekend) {
                $data[] = [
                    'saturday' => $weekend[0],
                    'sunday' => $weekend[1],
                    'city' => $this->getDoctrine()->getRepository('AppBundle:City')->getHottestCityForPeriod(
                        $weekend
                    ),
                ];
            }
            $view = $this->view($data, 200)->setContext($context);
        } elseif ($form->isSubmitted()) {
            $view = $this->view($form);
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Pager $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/weather-details", name="weather_details", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="List of weather details",
     *  authentication=false,
     *  parameters={
     *      {"name"="start_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     *      {"name"="end_date", "dataType"="string", "required"=false, "format"="2018-08-10"},
     *      {"name"="temperature", "dataType"="string", "required"=false, "description"="less or higher"},
     *      {"name"="temperature_direction", "dataType"="string", "required"=false, "description"="decimal number"},
     *  },
     *  statusCodes={
     *    200="Returns list of cities",
     *    400="Form errors"
     *  }
     * )
     */
    public function listWeatherDataAction(Request $request, Pager $paginator)
    {
        $context = new Context();
        $context->setGroups(['weather']);
        $context->enableMaxDepth();

        $form = $form = $this->createForm(WeatherDataFilterFormType::class);
        $form->handleRequest($request);

        if ($form->isValid() || !$form->isSubmitted()) {
            $weather = $this->getDoctrine()->getRepository('AppBundle:Weather')->listWeatherData($form->getData());
            $data = $paginator->paginate($weather, 2);
            $view = $this->view($data, 200)->setContext($context);
        } elseif ($form->isSubmitted()) {
            $view = $this->view($form);
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Pager $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/countries", name="countries", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="List of countries",
     *  authentication=false,
     *  statusCodes={
     *    200="Returns list of countries",
     *  }
     * )
     */
    public function countriesAction(Request $request, Pager $paginator)
    {
        $context = new Context();
        $context->setGroups(['country']);

        $countries = $this->getDoctrine()->getRepository('AppBundle:Country')->findAll();

        $data = $paginator->paginate($countries, 2);
        $view = $this->view($data, 200)->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Pager $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/cities", name="cities", methods={"GET"}, defaults={"_format"="json"})
     * @View()
     * @ApiDoc(
     *  section="Weather service",
     *  description="List of cities",
     *  authentication=false,
     *  statusCodes={
     *    200="Returns list of cities",
     *  }
     * )
     */
    public function citiesAction(Request $request, Pager $paginator)
    {
        $context = new Context();
        $context->setGroups(['city']);

        $cities = $this->getDoctrine()->getRepository('AppBundle:City')->findAll();

        $data = $paginator->paginate($cities, 2);
        $view = $this->view($data, 200)->setContext($context);

        return $this->handleView($view);
    }
}
