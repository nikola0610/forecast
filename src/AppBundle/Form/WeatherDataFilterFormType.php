<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Type;

class WeatherDataFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'start_date',
                TextType::class,
                array(
                    'constraints' => array(
                        new DateTime(array('format' => 'Y-m-d', 'message' => 'The start date is not valid.')),
                    ),
                )
            )
            ->add(
                'end_date',
                TextType::class,
                array(
                    'constraints' => array(
                        new DateTime(array('format' => 'Y-m-d', 'message' => 'The end date is not valid.')),
                    ),
                )
            )
            ->add(
                'temperature',
                TextType::class,
                array(
                    'constraints' => array(
                        new Type([
                            'type' => 'numeric',
                        ]),
                    ),
                )
            )
            ->add(
                'temperature_direction',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'less' => 'less',
                        'higher' => 'higher',
                    ),
                    'constraints' => array(
                        new Choice(
                            array(
                                'choices' => array(
                                    'less' => 'less',
                                    'higher' => 'higher',
                                ),
                                'message' => 'Invalid value. Available options are "less" and "higher"',
                            )
                        ),
                    ),
                )
            )
            ->setMethod(Request::METHOD_GET);
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            [$this, 'onPostSubmit']
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'allow_extra_fields' => true
            ]
        );
    }

    public function getBlockPrefix() {
        return null;
    }


    public function onPostSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        $startDate = $form['start_date']->getData();
        $endDate = $form['end_date']->getData();
        $direction = $form['temperature_direction']->getData();
        $temperature = $form['temperature']->getData();

        if ($direction && !$temperature) {
            $form['temperature']->addError(new FormError('Please add temperature.'));
        }

        if (!$direction && $temperature) {
            $form['temperature_direction']->addError(new FormError('Please add temperature comparison direction.'));
        }

        if ($startDate && !$endDate) {
            $form['end_date']->addError(new FormError('Please add end date.'));
        }
        if (!$startDate && $endDate) {
            $form['start_date']->addError(new FormError('Please add start date.'));
        }
    }
}
