<?php

namespace AppBundle\Service;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class Pager
 * @package AppBundle\Service
 */
class Pager
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var null|Request
     */
    private $request;

    /**
     * Pager constructor.
     * @param PaginatorInterface $paginator
     * @param RouterInterface $router
     * @param RequestStack $requestStack
     */
    public function __construct(PaginatorInterface $paginator, RouterInterface $router, RequestStack $requestStack)
    {
        $this->paginator = $paginator;
        $this->router = $router;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param $data
     * @param int $perPage
     * @return mixed
     */
    public function paginate($data, $perPage = 10)
    {
        $pagination = $this->paginator->paginate(
            $data,
            $this->request->query->getInt('page', 1),
            $perPage
        );
        $paginationData = $pagination->getPaginationData();

        if (isset($paginationData['next'])) {
            $nextUrl = $this->router->generate(
                $this->request->get('_route'),
                array_merge($this->request->query->all(), ['page' => $paginationData['next']]),
                UrlGeneratorInterface::ABSOLUTE_URL
            );
            $paged['next_page'] = $nextUrl;
        }
        if (isset($paginationData['previous'])) {
            $nextUrl = $this->router->generate(
                $this->request->get('_route'),
                array_merge($this->request->query->all(), ['page' => $paginationData['previous']]),
                UrlGeneratorInterface::ABSOLUTE_URL
            );
            $paged['previous_page'] = $nextUrl;
        }

        $paged['data'] = $pagination;

        return $paged;
    }
}