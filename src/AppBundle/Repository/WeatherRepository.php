<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class WeatherRepository
 * @package AppBundle\Repository
 */
class WeatherRepository extends EntityRepository
{
    /**
     * @param $data
     * @return mixed
     */
    public function listAverageTemperature($data)
    {
        $startDate = new \DateTime('-7 days');
        $endDate = new \DateTime();
        if ($data['start_date']) {
            $startDate = new \DateTime($data['start_date']);
        }
        if ($data['end_date']) {
            $endDate = new \DateTime($data['end_date']);
        }
        $query = $this->createQueryBuilder('p')
            ->select(['p as location', 'AVG(p.temperature) as temperature'])
            ->where('p.date >= :startDate')
            ->andWhere('p.date <= :endDate')
            ->setParameter('startDate', $startDate->format('Y-m-d'))
            ->setParameter('endDate', $endDate->format('Y-m-d'))
            ->groupBy('p.city');


        return $query->getQuery()->getResult();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function listWeatherData($data)
    {
        $startDate = new \DateTime('-7 days');
        $endDate = new \DateTime();
        if ($data['start_date']) {
            $startDate = new \DateTime($data['start_date']);
        }
        if ($data['end_date']) {
            $endDate = new \DateTime($data['end_date']);
        }
        $query = $this->createQueryBuilder('p')
            ->where('p.date >= :startDate')
            ->andWhere('p.date <= :endDate')
            ->setParameter('startDate', $startDate->format('Y-m-d'))
            ->setParameter('endDate', $endDate->format('Y-m-d'));


        if ($data['temperature'] && $data['temperature_direction'] == 'less') {
            $query->andWhere('p.temperature <= :temperature')
                ->setParameter('temperature', $data['temperature']);
        }

        if ($data['temperature'] && $data['temperature_direction'] == 'higher') {
            $query->andWhere('p.temperature >= :temperature')
                ->setParameter('temperature', $data['temperature']);
        }

        return $query->getQuery()->getResult();
    }
}
