<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    public function getHottestCityForPeriod($data)
    {
        $startDate = new \DateTime('-7 days');
        $endDate = new \DateTime();
        if ($data[0]) {
            $startDate = new \DateTime($data[0]);
        }
        if ($data[1]) {
            $endDate = new \DateTime($data[1]);
        }

        $query = $this->createQueryBuilder('c')
            ->select(['c as location', 'SUM(w.temperature)/2 as temperature'])
            ->join('c.weatherDetails', 'w')
            ->where('w.date >= :startDate')
            ->andWhere('w.date <= :endDate')
            ->setMaxResults(1)
            ->groupBy('c.id')
            ->orderBy('temperature', 'DESC')
            ->setParameter('startDate', $startDate->format('Y-m-d'))
            ->setParameter('endDate', $endDate->format('Y-m-d'));


        return $query->getQuery()->getResult();
    }
}
