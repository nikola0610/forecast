<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\Weather;
use AppBundle\Util\DataCollector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CollectWeatherDataCommand
 * @package AppBundle\Command
 */
class CollectWeatherDataCommand extends ContainerAwareCommand
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('app:collect-weather-data')
            ->setDescription('Collecting weather data');
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start');

        $locations = $this->getLocations();

        foreach ($locations as $location) {
            $response = DataCollector::getWeatherDataForLocation(
                $location->getCountry()->getCountryCode(),
                $location->getNameCanonical()
            );
            if ($response->getStatusCode() == Response::HTTP_OK) {
                $this->addWeatherData($location, json_decode($response->getBody()));
            }
        }

        $output->writeln('Data collected!');

    }

    /**
     * @return mixed
     */
    protected function getLocations()
    {
        $locations = $this->em->getRepository('AppBundle:City')->findAll();

        return $locations;
    }

    /**
     * @param City $city
     * @param $data
     */
    protected function addWeatherData(City $city, $data)
    {
        foreach ($data as $item) {
            if ($item) {
                $weatherData = $item->data[0];
                $weather = $this->em->getRepository('AppBundle:Weather')->findOneBy(
                    [
                        'date' => new \DateTime($weatherData->datetime),
                        'city' => $city,
                    ]
                );
                if (!$weather) {
                    $weather = new Weather();
                }
                $weather->setCity($city);
                $weather->setMaxTemperature($weatherData->max_temp);
                $weather->setMinTemperature($weatherData->min_temp);
                $weather->setTemperature($weatherData->temp);
                $weather->setDate(new \DateTime($weatherData->datetime));

                $this->em->persist($weather);
            }
        }
        $this->em->flush();
    }

}