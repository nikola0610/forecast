<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLocationData extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $countriesList = [
            ['Germany', 'Germany', 'DE'],
            ['Spain', 'Spain', 'ES'],
            ['Austria', 'Austria', 'AT'],
            ['Poland', 'Poland', 'PL'],
            ['Netherlands', 'Netherlands', 'NL'],
            ['United Kingdom', 'United Kingdom', 'UK'],
        ];
        $citiesList = [
            ['DE', 'Berlin', 'Berlin'],
            ['DE', 'Düsseldorf', 'Dusseldorf'],
            ['ES', 'Madrid', 'Madrid'],
            ['AT', 'Vienna', 'Vienna'],
            ['PL', 'Warsaw', 'Warsaw'],
            ['NL', 'Amsterdam', 'Amsterdam'],
            ['UK', 'London', 'London'],
        ];

        foreach ($countriesList as $countryItem) {
            $country = $manager->getRepository('AppBundle:Country')->findOneBy(['countryCode' => $countryItem[2]]);
            if (!$country) {
                $country = new Country();
            }
            $country->setCountryCode($countryItem[2]);
            $country->setName($countryItem[0]);
            $country->setNameCanonical($countryItem[1]);

            $manager->persist($country);
        }
        $manager->flush();

        foreach ($citiesList as $cityItem) {
            $city = $manager->getRepository('AppBundle:City')->findOneBy(['name' => $cityItem[2]]);
            if (!$city) {
                $city = new City();
            }
            $country = $manager->getRepository('AppBundle:Country')->findOneBy(['countryCode' => $cityItem[0]]);
            $city->setCountry($country);
            $city->setName($cityItem[1]);
            $city->setNameCanonical($cityItem[2]);

            $manager->persist($city);
        }

        $manager->flush();
    }
}
