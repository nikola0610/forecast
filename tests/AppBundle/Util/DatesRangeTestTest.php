<?php

namespace Tests\AppBundle\Util;

use AppBundle\Util\DatesRange;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DatesRangeTestTest extends WebTestCase
{
    /**
     * @dataProvider getDates
     */
    public function testGetWeatherDataForLocation($startDate, $ednDate)
    {
        $range = DatesRange::weekendFinder($startDate, $ednDate);
        $this->assertSame($range, [['2018-06-02','2018-06-03']]);
    }

    public function getDates()
    {
        yield ['2018-6-1', '2018-6-8'];
    }
}