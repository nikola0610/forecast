<?php

namespace Tests\AppBundle\Util;

use AppBundle\Util\DataCollector;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DataCollectorTest extends WebTestCase
{
    /**
     * @dataProvider getLocations
     */
    public function testGetWeatherDataForLocation($countryCode, $cityName)
    {
        $response = DataCollector::getWeatherDataForLocation($countryCode, $cityName);
        $this->assertSame($response->getStatusCode(), 200);
    }

    public function getLocations()
    {
        yield ['DE', 'Berlin'];
        yield ['ES', 'Madrid'];
    }
}