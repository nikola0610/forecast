<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


    public function testCities()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/cities');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCountries()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/countries');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testAverageTemperature()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/average-temperature');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testAverageTemperatureError()
    {
        $client = static::createClient();

        $crawler = $client->request(
            'GET',
            '/average-temperature?'.
            'start_date=123&'.
            'end_date=321'
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testWeekendPlanner()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/weekend-planner');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testListWeatherData()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/weather-details');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testListWeatherDataError()
    {
        $client = static::createClient();

        $crawler = $client->request(
            'GET',
            '/weather-details?'.
            'start_date=123&'.
            'end_date=321&'.
            'temperature=qwe&'.
            'temperature_direction=ewq'
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}
