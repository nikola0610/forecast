<?php
namespace ATests\AppBundle\Command;

use AppBundle\Command\CollectWeatherDataCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CollectWeatherDataCommandTest extends KernelTestCase
{
    public function testCollectWeatherData()
    {
        self::bootKernel();

        $container = self::$kernel->getContainer();
        $doctrine = $container->get('doctrine');
        $command = new CollectWeatherDataCommand($doctrine->getManager());
        $command->setApplication(new Application(self::$kernel));

        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
        $this->assertSame(0, $commandTester->getStatusCode());
    }
}